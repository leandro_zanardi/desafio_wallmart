var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var router = express.Router();
var mongoOp = require("./models/mongo");
var logistica = require("./logistica/melhor_caminho");

//aumenta o limite de request para 50 megas, possibilitando arquivos grandes de entrada
app.use(bodyParser.json({limit: '50mb'}));
//faz o parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({"extended" : false}));

//o route irah habilitar o mesmo path para diferentes tipos de rquests HTTP
//como POST, GET etc evitando assim tratamento redundante

// o request no barra apenas irah retornar uma lista com as possiveis api
router.get("/",function(req,res){
	res.json(
	{"erro":false, "mensagem":"Desafio Wallmart", "Use API":{
		"Listar mapas":"server:3000/mapas GET ",
		"Inseir novo mapa ou atualizar existente com mesmo nome":"server:3000/mapas POST application/json raw data {'nome':nome, 'malha':'A B 10 B C 20....'}",
		"Melhor Caminho":"server:3000/melhor_caminho POST application/json raw data {'mapa':'sao paulo','origem':'A','destino':'D','autonomia': 10,'valor_litro':2.5}"
		}
	});
});


router.route("/mapas")
	//lista todos os mapas
	.get(
		function(req,res){
			var response = {};
			mongoOp.find({}, function(err, data){
				//select full do mongo, apenas para fins de teste
				if(err){
					response = {"erro":true, "mensagem":"Erro buscando os dados" };
					res.status(500).json(response);
				}else{
					response = {"erro":false, "mensagem": data};
					res.json(response);
				}
				
			});
		}	
	)
	// insere um novo mapa
	.post(
		function(req,res){
			
			var response = {};
			var nome = req.body.nome;
			var malha_body = req.body.malha;
			if(nome == undefined || nome == null || nome == ""){
				response = {"erro":true, "mensagem":"nome do mapa invalido" };
				res.status(400).json(response);
			}else if(malha_body == undefined || malha_body == null || malha_body == ""){
				response = {"erro":true, "mensagem":"malha invalida" };
				res.status(400).json(response);
			}else{
				//verifica se o mapa existe no DB, caso sim faz update, caso nao cadastra
				mongoOp.findOne({"nome":nome}, function(err, data){
					if(err){
						response = {"erro":true, "mensagem":"Erro buscando os dados" };
						res.status(500).json(response);
					}else{
						if(data == null){
							var db = new mongoOp();
							response = { "erro":false, "mensagem":"Novo mapa inserido com sucesso."};
						}else{
							response = { "erro":false, "mensagem":"mapa existente substituido com sucesso"};
							db = data;
						}
						
						var malha = logistica.transformarMalha(malha_body);
						
						if(malha == false){
							response = {"erro":true, "mensagem":"formato de malha invalido" };
							res.status(400).json(response);
						}else{
							db.nome = nome;
							db.malha = malha;
							db.save( function(err){
								//save() vai rodar o insert() do mongo e vai inserir um novo mapa na colecao
								if(err){
									response = { "erro":true, "mensagem":"Erro adicionando novo mapa"};
									res.status(500).json(response);
								}
								res.json(response);
							});
						}
					}
				});
			}
		}
	);

router.route("/melhor_caminho")

	.get(function(req, res){
		
		var response = {"erro":true, "mensagem":"Esta API suporta apenas POST" };
		res.status(303).json(response);
		
	})
	
	//recebe uma chamada a API para identificar a melhor rota e custo
	.post(
		function(req, res){
				
			var mapa = req.body.mapa;
			var origem = req.body.origem;
			var destino = req.body.destino;
			var autonomia = req.body.autonomia;
			var valor_litro = req.body.valor_litro;
			
			if(mapa == undefined || mapa == null){
				response = {"erro":true, "mensagem":"mapa invalido" };
				res.status(400).json(response);
			}else if(origem == undefined || origem == null ){
				response = {"erro":true, "mensagem":"origem invalida" };
				res.status(400).json(response);
			}else if(destino == undefined || destino == null){
				response = {"erro":true, "mensagem":"destino invalido" };
				res.status(400).json(response);
			}else if(autonomia == undefined || autonomia == null || autonomia == 0){
				response = {"erro":true, "mensagem":"autonomia invalida" };
				res.status(400).json(response);
			}else if(valor_litro == undefined || valor_litro == null || valor_litro == 0){
				response = {"erro":true, "mensagem":"valor_litro invalido" };
				res.status(400).json(response);
			}else{
				mongoOp.findOne({"nome":mapa}, function(err, data){
					if(err){
						response = {"erro":true, "mensagem":"Erro buscando os dados" };
						res.status(500).json(response);
					}else{
						if(data == null){
							response = {"erro":true, "mensagem":"mapa nao encontrado" };
							res.status(400).json(response);
						}else{
							response = logistica.melhorCaminho(data.malha, origem, destino, autonomia, valor_litro);
							res.json(response);
						}
					}
				});
			}
		}
	);

app.use('/',router);
app.use(function(err, req, res, next) {
	  console.error(err.stack);
	  response = {"erro":true, "mensagem":"chamada invalida, o formato de entrada eh application/json" };
	  res.status(400).json(response);
});

//usando uma porta baixa, para evitar a exposicao da mesma, para que em ambiente produtivo responda apenas ip local
// atras de um proxy reverso para isso //TODO aceitar portas como parametro de inicializacao
app.listen(3000);
console.log("Listening to PORT 3000");