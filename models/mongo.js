
var mongoose = require("mongoose");
mongoose.connect( 'mongodb://127.0.0.1:27017/desafio_wallmart');

//TODO eventos de seguranca do DB

process.on('SIGINT', function() {  
  mongoose.connection.close(function () {
    console.log('Mongoose conexao fechada para node baixado');
    process.exit(0);
  });
});



//cria um schema e usa options de minimizar = false para poder armazenar objetos vazios
var mapaSchema = new mongoose.Schema({
		"nome": String,
		"malha": { type: mongoose.Schema.Types.Mixed, default: {} }
},{ minimize: false });


//cria o model caso nao exista
module.exports = mongoose.model('mapa', mapaSchema);

