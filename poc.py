#! /usr/bin/python
import time

def criar_mapa(nome,m):
    global mapa
    mapa['nome'] = nome
    malha = mapa['malha']
    entradas = m.split(" ")
    for p in range(0, len(entradas), 3) :
        if entradas[p] not in malha:
            malha[entradas[p]] = {}
        if entradas[p+1] not in mapa:
            malha[entradas[p+1]] = {}
        malha[entradas[p]][entradas[p+1]] = float(entradas[p+2])
    mapa['malha'] = malha    


def melhor_caminho(origem, destino):
    
    global mapa, autonomia, custo_litro, visitados, distancias, predecessores
    malha = mapa['malha']
    
    if origem == destino:
        rota = ""
        pred = destino
        while pred != None:
            rota = pred + " " + rota
            pred = predecessores.get( pred )
        print ( 'rota ' + rota + ' com custo de ' + str( (distancias[destino]/autonomia)*custo_litro ) )
    else:
        if not visitados:
            distancias[origem] = 0
        for vizinho in malha[origem]:
            if vizinho not in visitados:
                if origem not in distancias:
                    raise Exception( "destino inatingivel" )
                else:
                    nova_distancia = distancias[origem] + malha[origem][vizinho]
                    if nova_distancia < distancias.get( vizinho, float('inf') ):
                        distancias[vizinho] = nova_distancia
                        predecessores[vizinho] = origem
        
        visitados.append(origem)
        nao_visitados = {}
        for p in malha:
            if p not in visitados:
                nao_visitados[p] = distancias.get(p, float('inf'))
        menor_origem = min( nao_visitados, key=nao_visitados.get )
        melhor_caminho(menor_origem, destino )



if __name__ == "__main__":
    
    nome_mapa = "SP"
    malha_logistica = "A B 10 B D 15 A C 20 C D 30 B E 50 D E 30"
    mapa = {'nome':"", 'malha':{}}
    autonomia = 10
    custo_litro = 2.5
    visitados = []
    distancias = {}
    predecessores = {}
    current_milli_time = lambda: int(round(time.time() * 1000))
    
    a_time = current_milli_time()
    
    criar_mapa(nome_mapa, malha_logistica)
    print "malha logistica: " + malha_logistica
    print "mapa: " + str(mapa)
    
    melhor_caminho('A', 'D')
    
    b_time = current_milli_time()
    
    print "tempo total: " + str(b_time - a_time) + " ms"
