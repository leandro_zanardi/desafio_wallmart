

/**
 * 
 * Implementacao de Grafo com algoritmo Dijkstra
 * 
 * 
 */
var Graph = (function (undefined) {

	var extractKeys = function (obj) {
		var keys = [], key;
		for (key in obj) {
		    Object.prototype.hasOwnProperty.call(obj,key) && keys.push(key);
		}
		return keys;
	}

	var sorter = function (a, b) {
		return parseFloat (a) - parseFloat (b);
	}

	var findPaths = function (map, start, end, infinity) {
		infinity = infinity || Infinity;

		var costs = {},
		    open = {'0': [start]},
		    predecessors = {},
		    keys;

		var addToOpen = function (cost, vertex) {
			var key = "" + cost;
			if (!open[key]) open[key] = [];
			open[key].push(vertex);
		}

		costs[start] = 0;

		while (open) {
			if(!(keys = extractKeys(open)).length) break;

			keys.sort(sorter);

			var key = keys[0],
			    bucket = open[key],
			    node = bucket.shift(),
			    currentCost = parseFloat(key),
			    adjacentNodes = map[node] || {};

			if (!bucket.length) delete open[key];

			for (var vertex in adjacentNodes) {
			    if (Object.prototype.hasOwnProperty.call(adjacentNodes, vertex)) {
					var cost = adjacentNodes[vertex],
					    totalCost = cost + currentCost,
					    vertexCost = costs[vertex];

					if ((vertexCost === undefined) || (vertexCost > totalCost)) {
						costs[vertex] = totalCost;
						addToOpen(totalCost, vertex);
						predecessors[vertex] = node;
					}
				}
			}
		}

		if (costs[end] === undefined) {
			return null;
		} else {
			return predecessors;
		}

	}

	var extractShortest = function (predecessors, end) {
		var nodes = [],
		    u = end;

		while (u) {
			nodes.push(u);
			u = predecessors[u];
		}

		nodes.reverse();
		return nodes;
	}

	var findShortestPath = function (map, nodes) {
		var start = nodes.shift(),
		    end,
		    predecessors,
		    path = [],
		    shortest;

		while (nodes.length) {
			end = nodes.shift();
			predecessors = findPaths(map, start, end);

			if (predecessors) {
				shortest = extractShortest(predecessors, end);
				if (nodes.length) {
					path.push.apply(path, shortest.slice(0, -1));
				} else {
					return path.concat(shortest);
				}
			} else {
				return null;
			}

			start = end;
		}
	}

	var toArray = function (list, offset) {
		try {
			return Array.prototype.slice.call(list, offset);
		} catch (e) {
			var a = [];
			for (var i = offset || 0, l = list.length; i < l; ++i) {
				a.push(list[i]);
			}
			return a;
		}
	}

	var Graph = function (map) {
		this.map = map;
	}

	Graph.prototype.findShortestPath = function (start, end) {
		return findShortestPath(this.map, [start, end]);
	}


	return Graph;

})();




/**
 * Funcoes que serao exportadas ao modulo de logistica
 */
module.exports = {
	
	/**
	 *  Converte uma string de malha de logistica em objeto onde
	 *  cada origem eh uma propriedade objeto que contem propriedades destino vizinhos
	 *  com valor da distancia em km
	 */
	transformarMalha: function( m ){
		
		
		malha = {};
		entradas = m.split(" ");
		
		if(entradas.length%3 != 0){
			return false;
		}else{
		
			for( var x=0; x<entradas.length; x=x+3){
				if( malha[entradas[x]] == undefined){
					malha[entradas[x]] = {};
				}
				if( malha[entradas[x+1]] == undefined){
					malha[entradas[x+1]] = {};
				}
				malha[ entradas[x] ][ entradas[x+1] ] = parseFloat(entradas[x+2]);
			}
			return malha;
		
		}
		
	},

	/**
	 * 
	 * Funcao recursiva que retorna o melhor caminho entre origem e destino
	 * e calcula o preco em funcao da distancia / autonomia * preco do litro
	 * 
	 */
	melhorCaminho: function(malha, origem, destino, autonomia, valor_litro){
		

		//verifica se a origem e o destino sao validos
		if( malha[origem] == undefined ){
			response =  { "erro":true, "mensagem":"origem nao encontrada" };
		}else if(malha[destino] == undefined){
			response =  { "erro":true, "mensagem":"destino nao encontrado" };
		}else{
			
			//cria uma nova instancia de grafo com a malha
			graph = new Graph(malha);
			//encontra o melhor caminho
			var resposta = graph.findShortestPath(origem, destino);
			
			if(resposta == null){
				response =  { "erro":true, "mensagem":"destino inatingivel" };
			}else{
				//formata a saida
				str_path = "";
				var distancia = 0;
				for(var x=0; x<resposta.length; x++){
					str_path = str_path + (x!=0?" ":"") + resposta[x];
					if(x<resposta.length-1){
						distancia = distancia + parseFloat(malha[resposta[x]][resposta[x+1]]);
					}
				}
				
				var custo = ( distancia / autonomia ) * valor_litro;
				response =  { "erro":false, "rota":str_path, "custo":custo };
			}
			
		}
		
		return response;
		
	}

	
};

