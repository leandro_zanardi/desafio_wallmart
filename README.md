# README #

## Entregando mercadorias ##

* O Walmart esta desenvolvendo um novo sistema de logistica e sua ajuda é muito importante neste momento. Sua tarefa será desenvolver o novo sistema de entregas visando sempre o menor custo. Para popular sua base de dados o sistema precisa expor um Webservices que aceite o formato de malha logística (exemplo abaixo), nesta mesma requisição o requisitante deverá informar um nome para este mapa. É importante que os mapas sejam persistidos para evitar que a cada novo deploy todas as informações desapareçam.

* O formato de malha logística é bastante simples, cada linha mostra uma rota: ponto de origem, ponto de destino e distância entre os pontos em quilômetros.  A B 10 B D 15 A C 20 C D 30 B E 50 D E 30  Com os mapas carregados o requisitante irá procurar o menor valor de entrega e seu caminho, para isso ele passará o mapa, nome do ponto de origem, nome do ponto de destino, autonomia do caminhão (km/l) e o valor do litro do combustivel, agora sua tarefa é criar este Webservices. Um exemplo de entrada seria, mapa SP, origem A, destino D, autonomia 10, valor do litro 2,50; a resposta seria a rota A B D com custo de 6,25. Você está livre para definir a melhor arquitetura e tecnologias para solucionar este desafio, mas não se esqueça de contar sua motivação no arquivo README que deve acompanhar sua solução, junto com os detalhes de como executar seu programa.

* Documentação e testes serão avaliados também =) Lembre-se de que iremos executar seu código com malhas beeemm mais complexas, por isso é importante pensar em requisitos não funcionais também!!  Também gostariamos de acompanhar o desenvolvimento da sua aplicação através do código fonte. Por isso, solicitamos a criação de um repositório que seja compartilhado com a gente (de preferência um usuário e senha criado por você). Para o desenvolvimento desse sistema, nós solicitamos que você utilize a sua (ou as suas) linguagens de programação principal.

## Prova de Conceito ##

![IMG_0051.JPG](https://bitbucket.org/repo/6yLyMk/images/2261991617-IMG_0051.JPG)

Para a validação do algoritmo de melhor caminho, foi desenvolvida prova de conceito em python, prevendo a criação de mapa no modelo arquivo do MongoDB, JSON like.

Para executar a prova de conceito ( após a instalação do projeto ):


```
python poc.py
```

Esta irá retornar a saída:

```
malha logistica: A B 10 B D 15 A C 20 C D 30 B E 50 D E 30
mapa: {'malha': {'A': {'C': 20.0, 'B': 10.0}, 'C': {'D': 30.0}, 'B': {'E': 50.0, 'D': 15.0}, 'E': {}, 'D': {'E': 30.0}}, 'nome': 'SP'}
rota A B D  com custo de 6.25
tempo total: 0 ms
```

## Implantação ##

### MOTIVAÇÃO - Node.JS + Express + MongoDB ###

A motivação para implantação desta arquitetura foi:

* criar um webservice Rest API com baixo custo de memória para grande volume de requisições.
* Facilmente escalável horizontalmente sob um proxy reverso como NGINX.
* Persistência em banco de dados não relacional para possibilitar arquitetura distribuída em cloud como AWS, Azure e Google.
* Persistência dos mapas em arquivos JSON like, facilitando a manipulação de entrada e conversão para o formato de processamento, fazendo que a entrada do dado e seu processamento de pesquisa tenham um menor custo em relação à uma base normatizada.


## Requisitos ##

Requisitos para implantação do webservices REST API 
As requisições e respostas deverão conter o formato JSON

### Castrastrar / Atualizar mapa ###

URL POST http://localhost:3000/mapas
Formato de entrada:
```
{
"nome": "nome do mapa"
"malha": "malha logistica ex: A B 10..."
}
```

Saída esperada para novo mapa:
```
{ "erro":false, "mensagem":"Novo mapa inserido com sucesso."}
```

Saída esperada para mapa atualizado:
```
{ "erro":false, "mensagem":"mapa existente substituido com sucesso"}
```

Manter o arquivo de mapa no MongoDB:

Registro esperado no banco:
```
{
"_id": "56a64963f6075ff502e9e744",
"nome": "teste",
"__v": 0,
"malha": {
   "A": {
      "B": 10,
      "C": 20
    },
        "B": {
          "D": 15,
          "E": 50
        },
        "D": {
          "E": 30
        },
        "C": {
          "D": 30
        },
        "E": {}
      }
    }
```

### Consultar mapas (mostra todos os mapas, fins de estudo apenas)  ###

URL GET http://localhost:3000/mapas

Retorno esperado, lista de mapas do mongoDB:

```
{
  "erro": false,
  "mensagem": [
    {
      "_id": "56a4f8a25357c2b7044ebdc5",
      "nome": "sao paulo",
      "__v": 0,
      "malha": {
        "A": {
          "B": 10,
          "C": 20.........
```


### Solicitar melhor caminho ###

Dado um mapa, um ponto de origem e um ponto de destino, calcular a melhor rota.
Com a melhor rota, calcular o custo com relação à autonomia do veículo e o preço do litro de combustível.

Custo = Distancia em km / Autonomia em km/l * preco_litro

URL POST http://localhost:3000/melhor_caminho
Formato de entrada:
```
{
"mapa" : "teste",
"origem":  "A",
"destino" : "D",
"autonomia":10,
"valor_litro":2.5
}
```

Formato de saída:
```
{
  "erro": false,
  "rota": "A B D",
  "custo": 6.25
}
```



## Instalação ##

### Pré Requisitos ( Caso já possua Node e Mongo instalados, pular para Instalação do Projeto) ###

* [Node.JS ](https://nodejs.org/en/download/)

Exemplo instalação em Ubuntu 12.04

```
sudo apt-get install -y nodejs
```

* [MongoDB ](https://docs.mongodb.org/getting-started/shell/installation/)

Exemplo instalação Ubuntu 12.04

* Importar chave publica para ser usada no gerenciador de pacotes

```
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
```

* Criar uma source list

```
echo "deb http://repo.mongodb.org/apt/ubuntu precise/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
```

* Atualizar a lista

```
sudo apt-get update
```

* Instalar os pacotes

```
sudo apt-get install mongodb-org-server mongodb-org
```

* Subir o mongoDB

```
sudo service mongod start
```


## Instalação do Projeto ##

* Criar a base de dados desafio_wallmart

```
mongo
use desafio_wallmart
exit

```

* Executar a instalação do projeto e dependências, a partir do diretorio do projeto

```
npm install
```

* Iniciar o servico que subira na porta 3000

```
npm start
> desafio_wallmart@1.0.0 start /opt/desafio_wallmart
> node server.js

Listening to PORT 3000
```


### Execução ###

** Fluxo Positivo da aplicação - chamadas a API via Postman extensão para Chrome **

Chamada ao / lista as possíveis apis
```
http://localhost:3000
```
![Captura de Tela 2016-01-24 às 18.11.49.png](https://bitbucket.org/repo/6yLyMk/images/1256230599-Captura%20de%20Tela%202016-01-24%20a%CC%80s%2018.11.49.png)

Cadastrar novo mapa chamava via POST
```
http://localhost:3000/mapas
```
![Captura de Tela 2016-01-24 às 18.10.47.png](https://bitbucket.org/repo/6yLyMk/images/598939418-Captura%20de%20Tela%202016-01-24%20a%CC%80s%2018.10.47.png)

Listar todos os mapas ( apenas para propósito de testes ) chamada via GET
```
http://localhost:3000/mapas
```
![Captura de Tela 2016-01-24 às 18.11.13.png](https://bitbucket.org/repo/6yLyMk/images/4189992404-Captura%20de%20Tela%202016-01-24%20a%CC%80s%2018.11.13.png)

Calcular melhor caminho  chamada via POST
```
http://localhost:3000/melhor_caminho
```
![Captura de Tela 2016-01-24 às 18.11.58.png](https://bitbucket.org/repo/6yLyMk/images/3990769639-Captura%20de%20Tela%202016-01-24%20a%CC%80s%2018.11.58.png)




## Testes ##

Para rodar a cobertura de testes unitários:

* Iniciar o servico que subira na porta 3000

```
npm start
```

* Em uma nova janela execute os testes
```
npm test
```
Este devera reproduzir a saida

```

iMac-de-Leandro:desafio_wallmart leandrozanardi$ npm test

> desafio_wallmart@1.0.0 test /Users/leandrozanardi/Documents/workspace2/desafio_wallmart
> mocha test/routes.js

  precisa retornar lista de chamadas
{ erro: false,
  mensagem: 'Desafio Wallmart',
  'Use API': 
   { 'Listar mapas': 'server:3000/mapas GET ',
     'Inseir novo mapa ou atualizar existente com mesmo nome': 'server:3000/mapas POST application/json raw data {\'nome\':nome, \'malha\':\'A B 10 B C 20....\'}',
     'Melhor Caminho': 'server:3000/melhor_caminho POST application/json raw data {\'mapa\':\'sao paulo\',\'origem\':\'A\',\'destino\':\'D\',\'autonomia\': 10,\'valor_litro\':2.5}' } }
 
․precisa retornar mensagem de cadastro ou update
{ erro: false,
  mensagem: 'mapa existente substituido com sucesso' }
 
․precisa retornar erro 400 de nome do mapa invalido
{ erro: true, mensagem: 'nome do mapa invalido' }
 
․precisa retornar erro 400 de nome do malha invalida
{ erro: true, mensagem: 'malha invalida' }
 
․precisa retornar erro 400 de formato malha invalido
{ erro: true, mensagem: 'formato de malha invalido' }
 
․precisa retornar 303 se GET para api nao suportada
{ erro: true, mensagem: 'Esta API suporta apenas POST' }
 
․precisa retornar 400 mapa nao encontrado
{ erro: true, mensagem: 'mapa nao encontrado' }
 
․precisa retornar melhor caminho
{ erro: false, rota: 'A B D', custo: 6.25 }
 
․precisa retornar inatingivel
{ erro: true, mensagem: 'destino inatingivel' }
 
․precisa retornar origem nao encontrada
{ erro: true, mensagem: 'origem nao encontrada' }
 
․precisa retornar destino nao encontrado
{ erro: true, mensagem: 'destino nao encontrado' }
 


  11 passing (75ms)

```