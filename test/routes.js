var should = require('should'); 
var assert = require('assert');
var request = require('supertest');  


	var url = 'http://localhost:3000';
	
	/**
	 * 
	 *  COBERTURAS DE TESTES GENERICAS PARA A API
	 * 
	 */
	
	
	it('precisa retornar lista de chamadas', function(done) {
		
		
		request(url)
		.get('/')
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err, res) {
			if (err) {
			throw err;
			}
			console.log('precisa retornar lista de chamadas');
			should(res).have.property('status', 200);
			should(res.body).have.property('erro', false);
			console.log(res.body);
			console.log(" ");
			done();
		});
	});
	
	
	/**
	 * 
	 *  COBERTURA DE TESTES PARA API /mapas
	 * 
	 */
	
	
	
  
	it('precisa retornar mensagem de cadastro ou update', function(done) {
		
		var mapa = {
		nome: "teste",
		malha: "A B 10 B D 15 A C 20 C D 30 B E 50 D E 30"
		};
		  
		request(url)
		.post('/mapas')
		.send(mapa)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err, res) {
			if (err) {
			throw err;
			}
			console.log('precisa retornar mensagem de cadastro ou update');
			should(res).have.property('status', 200);
			should(res.body).have.property('erro', false);
			console.log(res.body);
			console.log(" ");
			done();
		});
	});
	
	
	it('precisa retornar erro 400 de nome do mapa invalido', function(done) {
		var mapa = {
				malha: "A B 10 B D 15 A C 20 C D 30 B E 50 D E 30"
		};
		request(url)
		.post('/mapas')
		.send(mapa)
		.expect('Content-Type', /json/)
		.expect(400) //Status code
		.end(function(err, res) {
			if (err) {
			throw err;
			}
			console.log('precisa retornar erro 400 de nome do mapa invalido');
			should(res.body).have.property('erro', true);
			console.log(res.body);
			console.log(" ");
			done();
		});
	});
	
	it('precisa retornar erro 400 de nome do malha invalida', function(done) {
		var mapa = {
				nome: "teste"
		};
		request(url)
		.post('/mapas')
		.send(mapa)
		.expect('Content-Type', /json/)
		.expect(400) //Status code
		.end(function(err, res) {
			if (err) {
			throw err;
			}
			console.log('precisa retornar erro 400 de nome do malha invalida');
			should(res.body).have.property('erro', true);
			console.log(res.body);
			console.log(" ");
			done();
		});
	});
	
	
	it('precisa retornar erro 400 de formato malha invalido', function(done) {
		var mapa = {
				nome: "teste",
				malha: "A B 10 B D 15 A C 20 C D 30 B E 50 D E 30 A"
		};
		request(url)
		.post('/mapas')
		.send(mapa)
		.expect('Content-Type', /json/)
		.expect(400) //Status code
		.end(function(err, res) {
			if (err) {
			throw err;
			}
			console.log('precisa retornar erro 400 de formato malha invalido');
			should(res.body).have.property('erro', true);
			console.log(res.body);
			console.log(" ");
			done();
		});
	});
	
	
	/**
	 * 
	 *  COBERTURA DE TESTES PARA API /melhor_caminho
	 * 
	 */
	
	
	it('precisa retornar 303 se GET para api nao suportada ', function(done) {
		request(url)
		.get('/melhor_caminho')
		.expect('Content-Type', /json/)
		.expect(303) //Status code
		.end(function(err, res) {
			if (err) {
			throw err;
			}
			console.log('precisa retornar 303 se GET para api nao suportada');
			should(res.body).have.property('erro', true);
			console.log(res.body);
			console.log(" ");
			done();
		});
	});
	
	
	it('precisa retornar 400 mapa nao encontrado ', function(done) {
		var mapa = {
					"mapa" : "teste bla bla",
					"origem":  "A",
					"destino" : "D",
					"autonomia":10,
					"valor_litro":2.5
		};
		request(url)
		.post('/melhor_caminho')
		.send(mapa)
		.expect('Content-Type', /json/)
		.expect(400) //Status code
		.end(function(err, res) {
			if (err) {
			throw err;
			}
			console.log('precisa retornar 400 mapa nao encontrado');
			should(res.body).have.property('erro', true);
			console.log(res.body);
			console.log(" ");
			done();
		});
	});
	
	it('precisa retornar melhor caminho', function(done) {
		var mapa = {
					"mapa" : "teste",
					"origem":  "A",
					"destino" : "D",
					"autonomia":10,
					"valor_litro":2.5
		};
		request(url)
		.post('/melhor_caminho')
		.send(mapa)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err, res) {
			if (err) {
			throw err;
			}
			console.log('precisa retornar melhor caminho');
			should(res.body).have.property('erro', false);
			console.log(res.body);
			console.log(" ");
			done();
		});
	});
	
	it('precisa retornar inatingivel', function(done) {
		var mapa = {
					"mapa" : "teste",
					"origem":  "D",
					"destino" : "A",
					"autonomia":10,
					"valor_litro":2.5
		};
		request(url)
		.post('/melhor_caminho')
		.send(mapa)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err, res) {
			if (err) {
			throw err;
			}
			console.log('precisa retornar inatingivel');
			should(res.body).have.property('erro', true);
			console.log(res.body);
			console.log(" ");
			done();
		});
	});
	
	
	it('precisa retornar origem nao encontrada', function(done) {
		var mapa = {
					"mapa" : "teste",
					"origem":  "G",
					"destino" : "A",
					"autonomia":10,
					"valor_litro":2.5
		};
		request(url)
		.post('/melhor_caminho')
		.send(mapa)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err, res) {
			if (err) {
			throw err;
			}
			console.log('precisa retornar origem nao encontrada');
			should(res.body).have.property('erro', true);
			console.log(res.body);
			console.log(" ");
			done();
		});
	});
	
	it('precisa retornar destino nao encontrado', function(done) {
		var mapa = {
					"mapa" : "teste",
					"origem":  "A",
					"destino" : "Z",
					"autonomia":10,
					"valor_litro":2.5
		};
		request(url)
		.post('/melhor_caminho')
		.send(mapa)
		.expect('Content-Type', /json/)
		.expect(200) //Status code
		.end(function(err, res) {
			if (err) {
			throw err;
			}
			console.log('precisa retornar destino nao encontrado');
			should(res.body).have.property('erro', true);
			console.log(res.body);
			console.log(" ");
			done();
		});
	});
	
	
	
	
	
	
	
